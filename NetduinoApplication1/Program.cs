﻿//using System;
//using System.Net;
//using System.Net.Sockets;
//using System.Threading;
//using Microsoft.SPOT;
//using Microsoft.SPOT.Hardware;
//using SecretLabs.NETMF.Hardware;
using SecretLabs.NETMF.Hardware.NetduinoPlus;
using System.Threading;
using WeatherStation.ILI9341;

namespace NetduinoApplication1
{
    public class Program
    {
        public static void Main()
        {
            // write your code here

            var tft = new ILI9341_TFTLCD(true);
            tft.ClearScreen();
            tft.DrawString(10, 10, "NETDUINO PLUS 2", 0xF800); 
            tft.DrawString(10, 50, "ILI9341 TFT TESTING PASS!", 0xF800);
            tft.DrawString(10, 90, "WWW.NTEX.TW", 0xF800); 
            tft.DrawString(10, 170, "WON'T SUPPORT CHINESE! @#$%^&*()_", 0xF800);
            tft.drawLine(10, 200, 200, 200, 0x0F00);
        }

    }
}
