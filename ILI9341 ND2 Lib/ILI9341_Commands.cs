﻿using System;
using Microsoft.SPOT;

namespace WeatherStation.ILI9341
{
    public class ILI9341_Commands
    {
        public const byte cmd_SoftwareReset = 0x01;
        public const byte cmd_EnterSleep = 0x10;
        public const byte cmd_ExitSleep = 0x11;
        public const byte cmd_GammaSet = 0x26;
        public const byte cmd_DisplayOff = 0x28;
        public const byte cmd_DisplayOn = 0x29;
        public const byte cmd_ColumnAddressSet = 0x2a;
        public const byte cmd_PageAddressSet = 0x2b;
        public const byte cmd_MemoryWrite = 0x2c;

        public const byte cmd_MemoryAccessControl = 0x36;
        public const byte cmd_PixelFormat = 0x3a;
        public const byte cmd_FrameControlNormal = 0xb1;
        public const byte cmd_FrameControlIdle = 0xb2;
        public const byte cmd_FrameControlPartial = 0xb3;
        public const byte cmd_DisplayFunctionControl = 0xb6;
        public const byte cmd_EntryModeSet = 0xb7;
        public const byte cmd_PowerControl1 = 0xc0;
        public const byte cmd_PowerControl2 = 0xc1;
        public const byte cmd_VCOM_Control_1 = 0xc5;
        public const byte cmd_VCOM_Control_2 = 0xc7;
        public const byte cmd_Positive_Gamma_Correction = 0xe0;
        public const byte cmd_Negative_Gamma_Correction = 0xe1;
    }
}
